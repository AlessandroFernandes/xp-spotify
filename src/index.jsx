import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import Store from './redux/Store';
import Routes from './route/routes';

import './assets/styles/reset.css';
import './assets/styles/normalize.css';
import './assets/styles/template/spotify.css';

const rootElement = document.getElementById('app');

ReactDOM.render(
    <Provider store={ Store }>
            <Routes />
    </Provider>,
rootElement);
