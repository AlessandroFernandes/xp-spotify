import React from 'react';

import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import { Card } from './card';

describe('Card', () => {
    it('render props', ()=> {
        const props = {
            artist: [],
            dispatch: jest.fn()
        }
        const wrapper = shallow(<Card {...props} />)
        expect(toJson(wrapper)).toMatchSnapshot()
    })
})