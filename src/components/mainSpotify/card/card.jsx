import React from 'react';
import { Link } from 'react-router-dom';

import { useDispatch } from 'react-redux';
import { SET_ALBUM, SET_ARTIST, SET_HISTORY } from '../../../redux/action.type';

import './card.css';

export const Card = ({ artist }) => {

    const dispatch = useDispatch();

    const setAlbum = () => { dispatch({ type: SET_HISTORY, history: artist}),
                             dispatch({ type: SET_ALBUM, album: artist }),
                             dispatch({ type: SET_ARTIST, album: [] }) };

    return(
        <div>
            {artist ?
                artist.images.length > 0 ?
                <img src={ artist.images[0].url } alt="artista" className="photo__api" /> :
                <div className="photo photo__api"></div>
                : <div className="photo photo__api"></div>
            }
            <div className="title">    
            { !artist ?
                <p>Nome do álbum</p>:
                <Link 
                    to={`/artist/${artist.id}`}
                    onClick={ setAlbum }
                >
                    { artist.name }
                </Link>
            }
            </div>    

        </div>
    )
}