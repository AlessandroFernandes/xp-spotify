import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { has } from 'lodash';

import SpotifyService from '../../service/spotifyService';
import { Card } from './card/card';
import { SET_TOKEN, SET_ARTIST } from '../../redux/action.type';
import { getToken } from '../../service/tokenController';

import Logo from '../../assets/image/logo.png';
import './mainSpotify.css';

export default MainSpotify => {
    const [name, setName] = useState('');

    const dispatch = useDispatch();
    const getTokenRedux = useSelector(state => state.tokenReducer);
    const getArtistRedux = useSelector(state => state.artistReducer.artist[0]);
    const getHistoryRedux = useSelector(state => state.historyReducer.history);
    
    const setTokenRedux = token => {
        dispatch({ type: SET_TOKEN, token })
    }

    const setArtistRedux = artist => {
        dispatch({ type: SET_ARTIST, artist })
    }

    useEffect(()=> { tokenController(), 
                     document.title= "PocSpotify - Main"
                   },[]);

    const tokenController = () => {
        const token = getToken();
        setTokenRedux(token);
    };

    const searchArtist = e => {
        setName(e.target.value);
        if(name.length > 2) {
            SpotifyService.SearchApi(name, getTokenRedux.token)
            .then(data => has(data, ['artists','items']) ? setArtistRedux(data.artists.items): '');
        }
    }

    return (
        <div className="container">
            <div className="main">
                <div className="main__logo">
                    <img src={ Logo } className="main__logo__brand" alt="logo Spotify"></img>
                </div>
                <header className="main__header">
                    <label className="main__header__label">Busque for artistas, álbuns ou músicas</label>
                    <input 
                        type="text" 
                        placeholder="Buscar..."
                        className="main__header__input"
                        value={ name }
                        onChange={ e => searchArtist(e) }
                    />
                </header>
                <main>
                    {
                         name === '' ?
                         <label className="main__main-label">Álbuns buscados recentemente</label>
                         :
                         <label className="main__main-label">Resultado encontrado para "{ name }"</label>
                    }
                   
                    
                </main>
                <div className="cards">
                    <div className="cards__main">
                        { getArtistRedux ?
                        getArtistRedux.map((artist, index) => {
                            return (
                                <ul className="cards__card" key={index}>
                                    <li>
                                            <Card artist={ artist }/>
                                    </li>
                                </ul>     
                            )   
                        }):
                            ""
                        }
                        { getHistoryRedux.length > 0 ?
                            getHistoryRedux.map((artist, index) => {
                                return (
                                    <ul className="cards__card" key={index}>
                                        <li>
                                                <Card artist={ artist }/>
                                        </li>
                                    </ul>     
                                )   
                            }):
                                <h1 className="card__msg">Busque seu artista favorito acima =)</h1>
                        }
                    </div>
                </div>
            </div>
        </div> 
    )
}
