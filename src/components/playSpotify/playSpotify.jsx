import React, { useEffect, useState, Fragment } from 'react';
import { Link } from 'react-router-dom';

import { useSelector } from 'react-redux';

import SpotifyService from '../../service/spotifyService'; 

import Logo from '../../assets/image/logoplay.png';
import './playSpotify.css';
export default function PlaySpotify(props) {
    const getAlbum = useSelector(state => state.albumReducer);
    const token = useSelector(state => state.tokenReducer);
    const [tracks, setTracks] = useState([]);

    useEffect(()=> { getTracks() }, []);

    const getTracks = () => {
        const query = props.match.params;
        SpotifyService.getTracks(token, query)
                      .then(data => setTracks(data.tracks))
    }

    return(
        <div className="container">
            <div className="main">
                <div className="main__logo">
                    <img src={ Logo } className="main__logo__brand" alt="logoSpotify"></img>
                </div>
                <div className="main__header">
                    <Link to="/main">&#60; Voltar</Link>
                </div> 
                <div className="play">
                    <div className="play__album">
                        <div className="play__album__itens">
                            { getAlbum.album && getAlbum.album[0].images[0] && getAlbum.album[0].images[0].url ?
                                <img src={ getAlbum.album[0].images[0].url } className="play__album__capa"></img>
                                :
                                <div className="play__album__capa-empty"></div>
                            }
                            
                            <p className="play__album__title">{ getAlbum.album[0].name }</p>
                         </div>
                    </div>
                    <div className="play__list">
                        { tracks ?
                            tracks.map((track, index) => {
                                return (
                                    <div className="play__list__player">
                                        <div key={ index }>
                                            <ul>
                                                <li>{ index + 1 }.</li>
                                                <li>{ track.name }</li>
                                            </ul>
                                            <audio autoPlay={false} controls>
                                                <source src={ track.preview_url } type="audio/mpeg" />
                                            </audio>
                                        </div>
                                    </div>
                                )
                            })
                          :
                            <p  className="card__msg">Nenhuma faixa encontrada =(</p>
                        } 
                    </div>
                </div>
               
            </div>

        </div>
    ) 
}