import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';

import auth from '../../config/auth';
import { hasToken } from '../../service/tokenController';

import Logo from '../../assets/image/logo.png';
import './home.css';
export default Home => {
    let token = '';
    
    useEffect(()=> { GetToken }, [] )

    const GetToken = () => {
        token = hasToken();
    }

    return (
        <div className="home">
            <main className="home__menu">
                <img src={ Logo} className="home__logo" title="Poc Spotify" alt="Poc Spotify" />
               
                {!token ?
                    <a 
                        href={`${auth.API_auth}client_id=${auth.clientID}&redirect_uri=${auth.redirectTo}&scope=${auth.scopes.join("%20")}&response_type=token&show_dialog=true`}
                        className="home__button"
                        title="Login">
                        Login
                    </a>:
                    <Link to="/main">Entrar</Link>
                }

            </main>
        </div>
    )
}