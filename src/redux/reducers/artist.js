import { SET_ARTIST } from '../../redux/action.type';

const inititalState = {
    artist: []
};

const reducer = (state = inititalState, action) => {
    switch(action.type) 
    {
        case SET_ARTIST:
            return { ...state, artist: [ action.artist ]}
        default:
            return state;
    }
}

export default reducer;