import { SET_HISTORY } from '../action.type';

const stateInitial = {
    history: []
};

const reducer = (state = stateInitial, action) => {
    switch(action.type) {
        case SET_HISTORY:
            return { ...state, history: [ ...state.history, action.history ]}
        default:
            return state
    }
}

export default reducer;