import { SET_ALBUM } from '../action.type';

const stateInitial = {
    album: []
}

const reducer = (state = stateInitial, action) => {
    switch(action.type) {
        case SET_ALBUM:
            return { ...state, album: [action.album]}
        default:
            return state
    }
}

export default reducer;