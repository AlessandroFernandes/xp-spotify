import { combineReducers } from 'redux';

import tokenReducer from './reducers/token';
import artistReducer from './reducers/artist';
import albumReducer from './reducers/album';
import historyReducer from './reducers/history';

export const Reducers = combineReducers({
  tokenReducer,
  artistReducer,
  albumReducer,
  historyReducer
});