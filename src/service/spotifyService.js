const URL_Spotify = "https://api.spotify.com/v1";

export default class SpotifyService 
{
    static SearchApi ( name, token) {
        return  fetch(`${URL_Spotify}/search?q=${ name }&type=artist`, {
                                headers: {
                                    "Authorization": `Bearer ${ token }`
                                }
                            })
                            .then(data => data.json())
                            .then(data => data)
                            .catch(error => { throw new Error, error})
    }

    static getTracks ( token, query) {

        return fetch(`https://api.spotify.com/v1/artists/${ query.id}/top-tracks?country=SE`, {
                        headers: {
                            "Authorization": `Bearer ${ token.token[0] }`
                        }
                    })
                    .then(data => data.json())
                    .then(data => data)
                    .catch(error => { throw new Error, error})
    }
}