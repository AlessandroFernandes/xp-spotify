export const getToken = () =>{
    const getHash = window.location.hash.substr(1);
    const hash = new URLSearchParams(getHash);
    const token = hash.get('access_token');
    setToken(token);
    return token;
};

const setToken = token => {
    let expire = new Date();
    expire.setMinutes(expire.getMinutes() + 60);
    window.cookie = `token=${ token }; expires=${ expire };`;
}

export const hasToken = () => {
    const getToken = window.cookie;
    if(getToken === undefined)
    {
        return '';
    }
    const token = getToken.substr(getToken.indexOf('token') + 6, 167);
    return token;
}