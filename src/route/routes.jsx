import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import { PrivateRoute } from './privateRoute';
import Home from '../components/home/home';
import MainSpotify from '../components/mainSpotify/mainSpotify';
import PlaySpotify from '../components/playSpotify/playSpotify';

const Router = {
    home:  { path: '/',     component: Home },
    main:  { path: '/main', component: MainSpotify },
    play:  { path: '/artist/:id', component: PlaySpotify },
    error: { path: '*',     component: Home }
}

export default Routes => {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path={ Router.home.path } component={ Router.home.component } />
                <Route path={ Router.main.path } component={ Router.main.component } />
                <PrivateRoute path={ Router.play.path } component={ Router.play.component } />
                <Route path={ Router.error.path } component={ Router.error.component } />
            </Switch>
        </BrowserRouter>
    )
}