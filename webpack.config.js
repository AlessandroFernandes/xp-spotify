const HtmlWebPackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: "./src/index.jsx",
    devServer: {
        inline: true,
        contentBase: './dist',
        compress: true,
        port: 4000,  
        historyApiFallback: true
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use:  { loader: "babel-loader"}
            },
            {
                test: /\.html$/,
                use: { loader: "html-loader"}
            },
            {
                test: /\.css$/i,
                use: ["style-loader", "css-loader"]
            },
            {
                test: /\.(png|jpe?g|gif)$/i,
                use: { loader: "file-loader"}
            }
        ]
    },
    resolve: {
        extensions: ["*", ".js", ".jsx", ".png"]
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: "./src/index.html",
            filename: "index.html"
        })
    ]
}